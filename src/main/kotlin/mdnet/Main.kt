/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet

import ch.qos.logback.classic.LoggerContext
import mdnet.logging.error
import org.slf4j.LoggerFactory
import picocli.CommandLine
import java.io.File
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Path

fun main(args: Array<String>) {
    CommandLine(Main()).execute(*args)
}

@CommandLine.Command(name = "java -jar <jar>", usageHelpWidth = 120, version = ["Client Version ${BuildInfo.VERSION} (Build ${Constants.CLIENT_BUILD})"])
class Main : Runnable {
    @field:CommandLine.Option(names = ["-s", "--settings"], defaultValue = ".\${sys:file.separator}settings.yaml", paramLabel = "<settings>", description = ["the settings file (default: \${DEFAULT-VALUE})"])
    lateinit var settingsFile: File
    @field:CommandLine.Option(names = ["-d", "--database"], defaultValue = ".\${sys:file.separator}", paramLabel = "<settings>", description = ["the database folder (default: \${DEFAULT-VALUE})"])
    lateinit var databaseFolder: Path
    @field:CommandLine.Option(names = ["-c", "--cache"], defaultValue = ".\${sys:file.separator}images", paramLabel = "<settings>", description = ["the cache folder (default: \${DEFAULT-VALUE})"])
    lateinit var cacheFolder: Path
    @field:CommandLine.Option(names = ["-h", "--help"], usageHelp = true, description = ["show this help message and exit"])
    var helpRequested: Boolean = false
    @field:CommandLine.Option(names = ["-v", "--version"], versionHelp = true, description = ["show the version message and exit"])
    var versionRequested: Boolean = false

    override fun run() {
        println(
            "Mangadex@Home Client Version ${BuildInfo.VERSION} (Build ${Constants.CLIENT_BUILD}) initializing"
        )
        println()
        println("Copyright (c) 2020, MangaDex Network")
        println(
            """
            Mangadex@Home is free software: you can redistribute it and/or modify
            it under the terms of the GNU General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            Mangadex@Home is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.

            You should have received a copy of the GNU General Public License
            along with Mangadex@Home.  If not, see <https://www.gnu.org/licenses/>.
            """.trimIndent()
        )

        if (!Files.isDirectory(databaseFolder) || Files.isRegularFile(databaseFolder.resolveSibling(databaseFolder.fileName.toString() + ".mv.db"))) {
            println()
            println()
            println(
                """the --database option now takes in the folder with the database file!
                |(it previously took in the path to the file without any extensions)
                |if you are using docker update your docker mount settings!
                |if you are not, manually move update your --database args!
                |note: the database file itself should be named metadata.{extension}
                |where {extension} can be `.db` or `.mv.db`
            """.trimMargin()
            )
            println()
            println()

            throw IllegalArgumentException()
        }

        if (Files.isRegularFile(databaseFolder)) {
            throw IllegalArgumentException("Database folder $databaseFolder must be a directory")
        }

        if (Files.isRegularFile(cacheFolder)) {
            throw IllegalArgumentException("Cache folder $cacheFolder must be a directory")
        }

        migrate(databaseFolder)

        val client = MangaDexClient(settingsFile, databaseFolder, cacheFolder)
        val hook = Thread {
            client.shutdown()
            (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        }
        Runtime.getRuntime().addShutdownHook(
            hook
        )

        try {
            client.runLoop()
        } catch (e: Throwable) {
            LOGGER.error(e) { "Failure when starting main loop" }
            Runtime.getRuntime().removeShutdownHook(
                hook
            )
            hook.run()
            throw e
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(Main::class.java)
    }
}
