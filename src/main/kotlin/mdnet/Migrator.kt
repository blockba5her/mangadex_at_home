/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet

import mdnet.cache.DbImage
import mdnet.cache.INIT_TABLE
import org.ktorm.database.Database
import org.ktorm.dsl.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

fun main() {
    migrate(Paths.get("./"))
}

fun migrate(path: Path) {
    val h2file = path.resolve("metadata.mv.db")
    if (!Files.exists(h2file)) {
        return
    }

    println("Migrating database - this may take a long time")

    Class.forName("org.sqlite.JDBC")

    val sqliteDb = path.resolve("metadata.db")
    Files.deleteIfExists(sqliteDb)

    val sqlite = Database.connect("jdbc:sqlite:$sqliteDb")
    sqlite.useConnection { conn ->
        conn.prepareStatement(INIT_TABLE).use {
            it.execute()
        }
    }

    val db = path.resolve("metadata")

    val h2 = Database.connect("jdbc:h2:$db")
    h2.useConnection { conn ->
        conn.prepareStatement(INIT_TABLE).use {
            it.execute()
        }
    }

    h2.from(DbImage).select().asIterable().chunked(1000).forEach { list ->
        sqlite.batchInsert(DbImage) {
            for (data in list) {
                item {
                    set(DbImage.id, data[DbImage.id])
                    set(DbImage.accessed, data[DbImage.accessed])
                    set(DbImage.size, data[DbImage.size])
                }
            }
        }
    }

    Files.move(h2file, path.resolve("metadata.mv.db.old"))
}
