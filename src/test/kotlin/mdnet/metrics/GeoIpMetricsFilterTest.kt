/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.metrics

import com.maxmind.geoip2.DatabaseReader
import com.maxmind.geoip2.model.CountryResponse
import com.maxmind.geoip2.record.Country
import io.kotest.core.spec.IsolationMode
import io.kotest.core.spec.style.FreeSpec
import io.micrometer.core.instrument.Counter
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.RequestSource
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.kotest.shouldHaveStatus
import java.net.InetAddress

class GeoIpMetricsFilterTest : FreeSpec() {
    override fun isolationMode() = IsolationMode.InstancePerTest

    init {
        val registry = mockk<PrometheusMeterRegistry>()
        val databaseReader = mockk<DatabaseReader>()

        val geoIpMetricsFilter = GeoIpMetricsFilter(databaseReader, registry)
        val filterRequest = geoIpMetricsFilter { Response(Status.OK) }

        "invalid source doesn't fail the image serving" {
            val address = "not a resolvable inetaddress"

            val request: Request = Request(Method.GET, "whatever")
                .source(RequestSource(address = address))

            val response = filterRequest(request)

            response.shouldHaveStatus(Status.OK)
        }

        "invalid header doesn't fail the image serving" {
            val address = "not a resolvable inetaddress"

            val request: Request = Request(Method.GET, "whatever")
                .header("X-Forwarded-For", address)

            val response = filterRequest(request)

            response.shouldHaveStatus(Status.OK)
        }

        "valid header and country resolved" {
            val address = "195.154.69.12"
            val countryCode = "COUNTRY_CODE"

            val countryResponse = mockk<CountryResponse>()
            val country = mockk<Country>()
            val counter = mockk<Counter>(relaxUnitFun = true)

            every { country.isoCode } returns countryCode
            every { countryResponse.country } returns country
            every { databaseReader.country(InetAddress.getByName(address)) } returns countryResponse
            every {
                registry.counter(
                    "requests_country_counts",
                    "country", countryCode
                )
            } returns counter

            val request: Request = Request(Method.GET, "whatever")
                .header("X-Forwarded-For", address)

            val response = filterRequest(request)

            response shouldHaveStatus Status.OK

            verify {
                registry.counter(
                    "requests_country_counts",
                    "country", countryCode
                )
            }
            confirmVerified(registry)

            verify {
                counter.increment()
            }
            confirmVerified(counter)
        }
    }
}
